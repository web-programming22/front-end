import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";



export const useProductStore = defineStore("Product", () => {
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({name: "", price: 0});



  async function getProducts() {
    loadingStore.isLongding = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLongding = false;
  }

  async function saavProduct(){
    loadingStore.isLongding = true;
    try{
      if(editedProduct.value.id){
        const res = await productService.updateProduct(editedProduct.value.id,editedProduct.value);
      }else{
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      clearForm();
      await getProducts();
    }catch (e){
      console.log(e)
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLongding = false;
  }

  function editProduct(product: Product){
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteProduct(id: number){
    loadingStore.isLongding = true;
    try{
      const red = await productService.deleteProduct(id); 
      await getProducts();
    }catch(e){
      console.log(e)
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLongding = false;
  }


  function clearForm(){
    editedProduct.value = {name: "", price: 0};
  }


  return { products, getProducts, dialog, editedProduct, saavProduct, editProduct, deleteProduct };
});
