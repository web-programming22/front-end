import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoadingStore = defineStore("loading", () => {
  const isLongding = ref(false);


  return { isLongding };
});
